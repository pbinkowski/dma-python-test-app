#!/usr/bin/env python3

import csv
import struct
from tqdm import tqdm
from tabulate import tabulate
from statistics import mean

from settings import *
from tests import *

for dma in dmas:
    dmaName = '{}-{}-{}'.format(dma[0], dma[1], dma[2])
    print('dma: {}'.format(dmaName))

    dev = open('/dev/dma-tester-{}'.format(dma[2]), 'rb+', 0)

    timings = {}
    dmaSummary = []

    for case in testCases:
        direction = S2MM if dma[1] == 's2mm' else MM2S
        width = case[0]
        height = case[1]
        stride = 0
        cmd = struct.pack(CFGFMT, direction, width, height, stride)
        caseName = '{}x{}'.format(case[0], case[1])
        caseConfigTimes = []
        caseTransferTimes = []
        for i in tqdm(range(RUNS), desc=caseName):
            dev.write(cmd)
            rsp = dev.read(struct.calcsize(RSPFMT))
            timing = struct.unpack(RSPFMT, rsp)
            caseConfigTimes.append(timing[0])
            caseTransferTimes.append(timing[1])

        avgCaseConfigTime = mean(caseConfigTimes)
        avgCaseTransferTime = mean(caseTransferTimes)
        avgCaseMBps = ((height * width * 4) / (1024 * 1024)) / (avgCaseTransferTime / 1000 / 1000 / 1000)

        timings[caseName] = (caseConfigTimes, caseTransferTimes)

        dmaSummary.append((caseName, avgCaseConfigTime, avgCaseTransferTime, avgCaseMBps))

    print(tabulate(dmaSummary, headers=['test case', 'config time (ns)', 'transfer time (ns)', 'mbps'], floatfmt='.2f'))

    dev.close()

    with open('out/' + dmaName + '.csv', mode='w') as resultsFile:
        resultsWriter = csv.writer(resultsFile)

        rows = []
        for i in range (RUNS + 1):
            rows.append([])

        for key, value in timings.items():
            rows[0] = rows[0] + [key, '']
            for i in range(RUNS):
                rows[i + 1] = rows[i + 1] + [value[0][i], value[1][i]]

        resultsWriter.writerows(rows)
