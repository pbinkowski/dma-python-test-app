dmas = [
#   name        , dir   , addr 
    ('vdma'     , 's2mm', '43c10000'),
    ('vdma'     , 'mm2s', '43c20000'),
    ('chisel'   , 'mm2s', '43c30000'),
    ('chisel'   , 's2mm', '43c40000'),
    ('dma-axi32', 'mm2s', '43c50000'),
    ('dma-axi32', 's2mm', '43c60000'),
    ('chisel'   , 'mm2s', '43c70000'),
    ('chisel'   , 's2mm', '43c80000'),
    ('wb-dma'   , 'mm2s', '43c90000'),
    ('wb-dma'   , 's2mm', '43ca0000'),
]

testCases = [
#   width, height
    (1024,    4),
    (1024,   16),
    (1024,   64),
    (1024,  256),
    (4   , 1024),
    (16  , 1024),
    (64  , 1024),
    (256 , 1024),
    (1024, 1024),
]

